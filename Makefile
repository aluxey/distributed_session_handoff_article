.PHONY: all clean html dvi dvi-lazy pdf pdf-lazy ps rtf text viewpdf viewdvi

LATEX        = latex
PDFLATEX     = pdflatex
TARGET       = main
SOURCE_FILES = $(TARGET).tex $(wildcard $(TARGET)*.tex)
PS2PDF       = ps2pdf -sPAPERSIZE=letter
FIGURES      = $(wildcard figs/*.{pdf,png,jpg}) # $(wildcard figs/*.png) $(wildcard fig/*.jpg)
BIB_FILES    = $(wildcard *.bib)

# Set the pdf reader according to the operating system
OS = $(shell uname)
ifeq ($(OS), Darwin)
	PDF_READER = open
endif
ifeq ($(OS), Linux)
	PDF_READER = evince
endif

all: pdf

pdf-lazy:
	-bibtex $(TARGET) > bibtex.log
	$(PDFLATEX) $(TARGET).tex | grep -A2 "^[^ \`]*:[0-9]*:.*\|^!" ; if [ $$? = 0 ] ; then false ; else true ; fi
	$(PDF_READER) $(TARGET).pdf
	-bibtex $(TARGET) > bibtex.log
#	$(LATEX) $(TARGET).tex
#	bibtex $(TARGET)

$(TARGET).pdf: $(SOURCE_FILES) $(BIB_FILES)
	$(PDFLATEX) --jobname=$(TARGET) $(SOURCE_FILES)
	-bibtex $(TARGET)
	$(PDFLATEX) --jobname=$(TARGET) $(SOURCE_FILES) # Bibtex ne marche pas sinon (!)
	$(PDFLATEX) --jobname=$(TARGET) $(SOURCE_FILES) # Bibtex ne marche pas sinon (!)

pdf: $(TARGET).pdf

clean:
	rm -f $(TARGET).ps $(TARGET).pdf $(TARGET).synctex.gz *.dvi *.aux *.bbl *.blg *.toc *.ind *.out *.brf *.ilg *.idx *.log
