# Reviewers commentaries sorted by feasibility

FYI: `Ri = Review #i` 

### Will be addressed:

* Adding a Related Work Section (R1, R3, R4)

* Estimate the approximate size of the session state record (R3)

* (Needs an approximate session size:) Relative cost of gossip messages over session chunks exchanges (R2) 

* Clarifications:
	
	[ ] Calculus of transition probabilities and how it survives incomplete sequences (R1)

	[x] Better justifications of choice of some parameters (sequence lengths) (R1, R4)

	[ ] Add model names to Fig. 5 (R1)

	[x] Chunks of a session: somehow already explained in II.C, but should come earlier (i.e. II.A) (R2)

	[ ] Comparison against centralized service: explain that they score Waiting time = 1 and Proactive network cost = 0 (R3)

	[x] More details about the simulation

* Typos, English (R4)


### Can easily be addressed:

* Compare gossip information dissemination with broadcast (R2)

### Cannot be easily addressed:

* Evaluation with real-world data (R1, R3)
* Measure actual latencies (in milliseconds) of the handoff: our session handoff is simulated, it would be another approximation (R3)
* Comparison against equivalent systems providing unified experience across multiple devices (R4)

### Will not be addressed:

* Experiment probabilistic broadcast with different fanouts: [Kermarrec, Massoulié, Ganesh 2000] *proved* that `f>=log(N)` did the job. (R2)

### Future work / Interesting suggestions:

* Testbed with more realistic data (R1)
* Real chunking à la BitTorrent is already part of a new submission (R2)
* Taking advantage of physically co-located devices for broadcast (among other points of interest) (R2)
* Adapting to change of the user's behavior (R2)

### Positive feedback:

* Well written (R1)
* Distributed is fashionable (R2, R4)
* Evaluation clearly shows tradeoffs of the approach w.r.t itself. That is, Figure 7 is good (R2)