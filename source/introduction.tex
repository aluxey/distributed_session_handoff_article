%!TEX root = main.tex


\section{Introduction}

%% Nowadays, we live in a connected world. 
These last decades have witnessed an exponential proliferation of devices connected to the Internet. We are entering a new era, a further step towards Mark Weiser's vision of ubiquitous computing \cite{Weiser1997}, where the paradigm of a single user for a single device no longer applies. Users own multiple devices (PCs, smartphones, smart watches, tablets, notebooks), and no longer spend their time on a single desktop screen to perform their daily digital activities
% Users are evolving in a multi-device ecosystem; they are surrounded by a set of interconnected devices.
%% As a result, to carry out their activities,
(browsing, searching, online shopping and gaming, video streaming)~%% , etc., users' interactions are spanned across multiple devices according to the context
\cite{google2012%% }. According to their amount of time, location, attitude, and/or state of mind, users are sequentially switching among their devices to achieve their goal \cite{
,Kawsar2013}.

This emerging trend is far from being without challenges. Accessing everything, anytime, anywhere, in a continuous manner across various heterogeneous devices at the right time and at the right place, may become a daunting task for users. Particularly, support for streamlining the user experience is lagging behind. 
As introduced by Levin~\cite{Levin2014}, applications targeting a multi-device ecosystem should be designed with three key fundamental concepts in mind: \emph{Consistency}, \emph{Continuity}, and \emph{Complementarity} (for short the \emph{3Cs}). %, in order to provide a successful multi-device user experience. 
For instance, one application may not have a \textit{consistent} user interface across heterogeneous devices, undermining the user's multi-device interaction, and hence increasing user frustration. Similarly, with applications not designed for \textit{continuity}, users may have to manage themselves how to resume their interaction context from their current device to the next one. 
This implies, most often, that users have to manually transfer up-to-date application data, back and forth across devices, in order to recreate interaction sessions, thus compromising the fluidity of interaction. Finally, multiple devices may be used simultaneously to  \textit{complement} each other, e.g., one device may act as a remote control to pilot another device. 
%Over the last years, in the Human Computer Interaction (HCI) research field, a lot of interests have been put to outline challenges to design applications enabling a fluid user interaction spanning heterogeneous devices, and to understand the current multi-device experience of users. 

This paper explicitly targets the issue of providing \emph{continuous} interaction. Indeed, with the latest advances in Web technologies, %% and engineering
 we consider that the problem of designing \textit{consistent} applications is almost solved. In particular, through the use of \textsc{HTML}, \textsc{CSS}, and \textsc{Javascript} natively embedded in 
Web browsers, web-based applications have become mostly consistent across heterogeneous devices%% with a single code base,
%% independently of the platform at hand
~\cite{phonegap,ionic,cordova}, and are now 
%% As a result, Web-based applications become
the most adequate candidates to support users' multi-device experience~\cite{MikkonenSP15, Bellucci:sigchi2011, Lo:www2013, Oh:vee2015, Hydrascope2013, Weave15, Husmann:2016}. 

Additionally, from a recent study led by Google~\cite{google2012}, $90\%$ of users who grew up in a multi-device ecosystem, switch between devices in a sequential way. 
In other terms, a given application may only be active on a single device at a given point in time. 
The prevalence of sequential usage leads us: (i) to focus our work on sequential interactions, and (ii) to consider the simultaneous usage of devices, i.e the \textit{complementary} aspect of users' interaction, for future works. 

Finally, the problem of providing continuous interaction across heterogeneous devices is currently massively addressed by relying \textit{de facto} on cloud providers that act as a central point of synchronization. For instance, popular applications such as \textsc{Evernote}\cite{evernote}, \textsc{1Password}\cite{1password}, \textsc{Wunderlist}\cite{wunderlist}, \textsc{Chrome}\cite{chrome}, \textsc{Amazon Kindle}\cite{kindle}, etc. rely on either \textsc{Google Drive}, \textsc{Dropbox}, \textsc{iCloud}, \textsc{Amazon S3} and/or their own servers to perform \emph{session handoff}, i.e to save and transfer the interactive session of an application in the multi-device ecosystem. 
However, such solutions suffer from a key shortcoming: users' activity and their related personal and private information are recorded by application providers. Users do not expect to have their digital life tracked and analyzed by a third party that collects data on their behalf. 

To the best of our knowledge, only few approaches have sought to protect users' privacy in a multi-device ecosystem. 
These approaches typically exploit a peer-to-peer architecture to save and transfer application sessions only on devices owned by users, avoiding the need to trust any third party \cite{Fisher:2014, Melchior:2009,GallidabinoP16}. 
However, as the behavior of users is not known in advance, the aforementioned solutions blindly flood the ongoing interactive session to all devices of the ecosystem, as no device is able to predict which device will be used next.
%will be the next device used in the interaction sequence of the user. 
Such a brute-force broadcast mechanism, despite its simplicity, leads to very poor performances as it implies: (i) redundant messages, (ii) overconsumption of network bandwidth, (iii) a higher latency that inherently impacts the fluidity of user interaction, and (iv) faster energy depletion. 
Further, it does not scale well with the number of devices. Although the current number of devices owned by a user is on average around 4 devices \cite{google2012}, this number is expected to increase with the advent of the Internet Of Things. 
%% Industry leaders such as Cisco or Intel predict that there would be 50 billion of connected objects by 2020. It is highly likely that part of these devices will extend the multi-device ecosystem, turning the use of blind flooding definitely inapplicable. 
%\\

In this paper, we introduce \algorithmName, a novel approach to perform predictive session handoff by learning in a \emph{decentralized} manner how the user behaves.  
\algorithmName combines a probabilistic dissemination protocol and a proactive session handoff mechanism in order to provide seamlessly fluid user interactions in a multi-device ecosystem. Our solution: (i) does not rely on any centralization point, (ii) has a bounded and known network resource consumption, (iii) is able to predict which device is the most likely to be used next, enabling the transfer of the ongoing interaction session without blind flooding, (iv) respects the user's right to privacy, and (v) scales to an arbitrary number of devices. 

Our contributions are as follows:
\begin{itemize}
\item We have designed \algorithmName, a protocol based on two algorithms: the \textit{\algorithmName Gossiper}, which allows devices to gain knowledge of the user’s behavior in a distributed manner; and the \textit{\algorithmName Session Handoff} mechanism, which uses this knowledge to proactively send chunks of the current session to devices that will most probably be used next;
\item We have evaluated our approach with 8 different discrete time Markov models to emulate user behaviors;
\item We have demonstrated that there is no ideal dissemination protocol. It is all about the tradeoff between prediction accuracy, latency, and network consumption. We show in particular that the performance of \algorithmName greatly depends on the user’s behavior. The more predictable a user is, the better \algorithmName performs;
\item \algorithmName allows designers to efficiently trade off network costs for fluidity, and is for instance able to reduce network costs by up to 80\% against a flooding strategy while maintaining a fluid user experience.
  % When predictions are accurate, the user’s experience is greatly enhanced, since she has nearly no time to wait to obtain a seamless interaction with her various devices.
\end{itemize}

%The remainder of this paper is structured as follows. %% Section~\ref{related} introduces the related work.
In the following we detail \algorithmName's concepts and approach (Section~\ref{sec:concepts_overall_approach}). Section~\ref{sec:evaluation} then evaluates the proposed approach regarding performance and different models emulating different types of user behavior. Finally, Section~\ref{related} considers related work, and Section~\ref{sec:concl-future-work} discusses future work and conclusions. 
